//express server
const bodyParser = require("body-parser");
const mongoose = require("mongoose");
const express = require("express");
const app = express();

// const api = require("./routes/api/api");

//body parser MW
app.use(bodyParser.json());

// import database info
const db = require("../server/config/keys").mongoURI;

//connect to db
mongoose
  .connect(db, { useNewUrlParser: true, useUnifiedTopology: true })
  .then(() => console.log("Connected to DB"))
  .catch(err => console.log(err));

//Redirect Routes
// app.use("/api/", api);

// set port
const port = process.env.PORT || 5000;

//listen for server
app.listen(port, () => console.log(`Server started on port ${port}`));
