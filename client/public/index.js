import React, { setGlobal } from "reactn";
import ReactDOM from "react-dom";
import App from "../src/app.js";

setGlobal({
  fromIndexJS: "This is global state from index.JS"
});

ReactDOM.render(<App />, document.getElementById("root"));

if (module.hot) {
  module.hot.accept();
}
