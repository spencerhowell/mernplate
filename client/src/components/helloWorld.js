import React, { Component, setGlobal } from "reactn";

export class HelloWorld extends Component {
  componentDidMount() {
    setGlobal({ myData: "This is in global state" });
  }

  render() {
    return (
      <div>
        <h1>Hello World!</h1>
        <h1>Local state: {this.global.myData}</h1>
        <h2>Global State: {this.global.fromIndexJS}</h2>
      </div>
    );
  }
}

export default HelloWorld;
